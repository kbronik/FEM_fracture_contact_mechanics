#pragma once

//-----------------------------------------------------------------------------
struct FETimePoint
{
	double	t;		// current time value
	double	dt;		// current time step (distance between this time and previous one)
};
