#pragma once
#include "FECoreBase.h"

//-----------------------------------------------------------------------------
//! This class can be used to define global model data and will be placed in the
//! global date section of the FEModel class
class FEGlobalData : public FECoreBase
{
public:
	//! constructor
	FEGlobalData();
};
