# Make include file for FEBio on Linux

include $(FEBDIR)build/make.lnx64d

CC = g++

FLG = -O3 -fPIC

INC = -I$(FEBDIR) -I$(FEBDIR)build/include
