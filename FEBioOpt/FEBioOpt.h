#pragma once

//-----------------------------------------------------------------------------
//! The FEBioOpt module 

//! The FEBioOpt module solves parameter optimization problems with FEBio
//!
namespace FEBioOpt {

void InitModule();

}
