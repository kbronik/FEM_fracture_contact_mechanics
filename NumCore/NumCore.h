#pragma once

#include "FECore/LinearSolver.h"

namespace NumCore {

LinearSolver* CreateLinearSolver(int ntype);

} // namespace NumCore
