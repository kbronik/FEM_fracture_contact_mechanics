//! This implementation of the Pardiso solver is for the shared object
//! library available at http://www.pardiso-project.org

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include "PardisoSolver.h"

#ifndef PARDISO

//////////////////////////////////////////////////////////////
// PardisoSolver
//////////////////////////////////////////////////////////////

PardisoSolver::PardisoSolver()
{
	/* Make sure the solver is available */
#ifndef PARDISODL
	fprintf(stderr, "FATAL ERROR: The Pardiso solver is not available on this platform\n\n");
	exit(1);
#endif
}

bool PardisoSolver::PreProcess()
{
	/* Make sure the solver is available */
#ifndef PARDISODL
	fprintf(stderr, "FATAL ERROR: The Pardiso solver is not available on this platform\n\n");
	return false;
#else
	m_mtype = (m_bsymm ? -2 : 11); /* Real symmetric matrix */
	m_iparm[0] = 0;
	int solver = 0; /* 0 - sparce direct solver, 1 - multi-recursive iterative solver */
	pardisoinit_(m_pt, &m_mtype, &solver, m_iparm, m_dparm, &m_error);

	if (m_error)
	{
		if (m_error == -10) fprintf(stderr, "No license file found\n\n");
		if (m_error == -11) fprintf(stderr, "License is expired\n\n");
		if (m_error == -12) fprintf(stderr, "Wrong username or hostname\n\n");
		return false;
	}
	//else fprintf(stderr, "PARDISO license check was successful\n\n");

	m_n = m_pA->Size();
	m_nnz = m_pA->NonZeroes();
	m_nrhs = 1;

	// number of processors: use value of OMP_NUM_THREADS
	m_iparm[2] = m_numthreads;

	m_maxfct = 1;	/* Maximum number of numerical factorizations */
	m_mnum = 1;	/* Which factorization to use */

	m_msglvl = 0;	/* 0 Suppress printing, 1 Print statistical information */

	return LinearSolver::PreProcess();
#endif
}

bool PardisoSolver::Factor()
{
	/* Make sure the solver is available */
#ifndef PARDISODL
	fprintf(stderr, "FATAL ERROR: The Pardiso solver is not available on this platform\n\n");
	return false;
#else
	CompactMatrix* A = dynamic_cast<CompactMatrix*> (m_pA);

// ------------------------------------------------------------------------------
// Reordering and Symbolic Factorization.  This step also allocates all memory
// that is necessary for the factorization.
// ------------------------------------------------------------------------------

	int phase = 11;

	pardiso_(m_pt, &m_maxfct, &m_mnum, &m_mtype, &phase, &m_n, A->Values(), A->Pointers(), A->Indices(),
		 NULL, &m_nrhs, m_iparm, &m_msglvl, NULL, NULL, &m_error, m_dparm);

	if (m_error)
	{
		fprintf(stderr, "\nERROR during symbolic factorization: ");
		print_err();
		exit(2);
	}

// ------------------------------------------------------------------------------
// This step does the factorization
// ------------------------------------------------------------------------------

	phase = 22;

#ifdef PRINTHB
	A->print_hb();
#endif

	pardiso_(m_pt, &m_maxfct, &m_mnum, &m_mtype, &phase, &m_n, A->Values(), A->Pointers(), A->Indices(),
		 NULL, &m_nrhs, m_iparm, &m_msglvl, NULL, NULL, &m_error, m_dparm);

	if (m_error)
	{
		fprintf(stderr, "\nERROR during factorization: ");
		print_err();
		exit(2);
	}

	return true;
#endif
}

bool PardisoSolver::BackSolve(vector<double>& x, vector<double>& b)
{
	/* Make sure the solver is available */
#ifndef PARDISODL
	fprintf(stderr, "FATAL ERROR: The Pardiso solver is not available on this platform\n\n");
	return false;
#else
	CompactMatrix* A = dynamic_cast<CompactMatrix*> (m_pA);

	int phase = 33;

	m_iparm[7] = 1;	/* Maximum number of iterative refinement steps */

	pardiso_(m_pt, &m_maxfct, &m_mnum, &m_mtype, &phase, &m_n, A->Values(), A->Pointers(), A->Indices(),
		 NULL, &m_nrhs, m_iparm, &m_msglvl, &b[0], &x[0], &m_error, m_dparm);

	if (m_error)
	{
		fprintf(stderr, "\nERROR during solution: ");
		print_err();
		exit(3);
	}

	return true;
#endif
}

void PardisoSolver::Destroy()
{
	/* Make sure the solver is available */
#ifndef PARDISODL
	fprintf(stderr, "FATAL ERROR: The Pardiso solver is not available on this platform\n\n");
	exit(1);
#else

	CompactMatrix* A = dynamic_cast<CompactMatrix*> (m_pA);

	int phase = -1;

	pardiso_(m_pt, &m_maxfct, &m_mnum, &m_mtype, &phase, &m_n, NULL, A->Pointers(), A->Indices(),
		 NULL, &m_nrhs, m_iparm, &m_msglvl, NULL, NULL, &m_error, m_dparm);

	LinearSolver::Destroy();

#endif
}
#endif
