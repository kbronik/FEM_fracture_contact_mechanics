#pragma once

//-----------------------------------------------------------------------------
//! The FEBioMix module 

//! The FEBioMix module adds mixture capabilites to FEBio, including biphasic,
//! biphasic-solute, multiphasic, charged solutes and chemical reations.
//!
namespace FEBioMix {

void InitModule();

}
