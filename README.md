[![GitHub issues](https://img.shields.io/github/issues/kbronik2017/FEM_fracture_contact_mechanics)](https://github.com/kbronik2017/UFEM_fracture_contact_mechanics/issues)
[![GitHub forks](https://img.shields.io/github/forks/kbronik2017/FEM_fracture_contact_mechanics)](https://github.com/kbronik2017/FEM_fracture_contact_mechanics/network)
[![GitHub stars](https://img.shields.io/github/stars/kbronik2017/FEM_fracture_contact_mechanics)](https://github.com/kbronik2017/FEM_fracture_contact_mechanics/stargazers)


<br>
 <img height="310" src="images/tear.gif"/>
</br>


## A modification of the classical contact to provide a specific implementation of a mesh-independent treatment for straightforward modelling of (non-linear) fracture mechanical processes using a mixed mode cohesive zone method
 <br>
<img src="images/largedeformation.png" alt="Example" width="2000" height="300" />
 </br>
 
 ### This is an extended version of open source code  FEBio  https://febio.org/ (version 2.0) which deals with the following problems:


- Modification and implementation of large deformation contact mechanics using a mixed mode cohesive zone model (development of a cohesive fracture model through contact mechanics)


- Incorporation of friction and frictional heat generation in the contact model (consideration of interfacial representations of total energy balance and entropy production) 


## Publication

https://www.morebooks.de/store/gb/book/large-deformation-cohesive-damage-model-through-contact-mechanics/isbn/978-613-9-92761-6
 <br>
 <img src="images/pub.jpg" alt="Example" width="1000" height="700" />
 </br>

## Mathematical and mechanical description(Kevin Bronik):
 <br>
 <img src="images/page1.jpg" alt="Example" width="2500" height="1500" />
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
 <img src="images/page2.jpg" alt="Example" width="2500" height="1500" />
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
 <img src="images/page3.jpg" alt="Example" width="2500" height="1500" />
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
 <img src="images/page4.jpg" alt="Example" width="2500" height="1500" />
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
 <img src="images/page5.jpg" alt="Example" width="2500" height="1500" />
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
 <img src="images/page6.jpg" alt="Example" width="2500" height="1500" />
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
 <img src="images/page7.jpg" alt="Example" width="2500" height="1500" />
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
 <img src="images/page8.jpg" alt="Example" width="2500" height="1500" />
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
 <img src="images/page9.jpg" alt="Example" width="2500" height="1500" />
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
 <img src="images/page10.jpg" alt="Example" width="2500" height="1500" />
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
 <img src="images/page11.jpg" alt="Example" width="2500" height="1500" />
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
 <img src="images/page12.jpg" alt="Example" width="2500" height="1500" />
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
 <img src="images/page13.jpg" alt="Example" width="2500" height="1500" />
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
 <img src="images/page14.jpg" alt="Example" width="2500" height="1500" />
 </br>
