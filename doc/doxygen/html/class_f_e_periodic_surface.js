var class_f_e_periodic_surface =
[
    [ "FEPeriodicSurface", "class_f_e_periodic_surface.html#a2726f5d9e385735f04eb91c89109a3bf", null ],
    [ "CenterOfMass", "class_f_e_periodic_surface.html#a5987ecc27b7e2576cc67ac4fc0a4ea4c", null ],
    [ "Init", "class_f_e_periodic_surface.html#a2848d9c75895970e226db691df2c07fd", null ],
    [ "Serialize", "class_f_e_periodic_surface.html#aef29064bf4c9d7dd04a937c3dc5ea8cf", null ],
    [ "ShallowCopy", "class_f_e_periodic_surface.html#acaa39bebdcc46068b5cff0b41c9b6f77", null ],
    [ "Update", "class_f_e_periodic_surface.html#afe6e2b41c0e034679ec830c26da81997", null ],
    [ "m_gap", "class_f_e_periodic_surface.html#a83a7ab5c3e22f05facea59b08ffbd538", null ],
    [ "m_Lm", "class_f_e_periodic_surface.html#ae60676a424b2b420e2cf8bed9326e64a", null ],
    [ "m_pme", "class_f_e_periodic_surface.html#ae3fea11abc70f3dbf3c8ca2607dae879", null ],
    [ "m_rs", "class_f_e_periodic_surface.html#ad1b3198a84df802a6bbf2d1c2ece4237", null ]
];