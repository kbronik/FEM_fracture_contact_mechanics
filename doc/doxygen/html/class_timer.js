var class_timer =
[
    [ "Timer", "class_timer.html#a5f16e8da27d2a5a5242dead46de05d97", null ],
    [ "GetTime", "class_timer.html#a99983986660f2cc9153140531fbec40b", null ],
    [ "GetTime", "class_timer.html#ada89c458b1ef47321f694946b46ab0cf", null ],
    [ "peek", "class_timer.html#a0b989c5aadd12b418f495f36fc561b29", null ],
    [ "reset", "class_timer.html#a9020542d73357a4eef512eefaf57524b", null ],
    [ "start", "class_timer.html#a3a8b5272198d029779dc9302a54305a8", null ],
    [ "stop", "class_timer.html#a63f0eb44b27402196590a03781515dba", null ],
    [ "time_str", "class_timer.html#a8f6f684ea7bed8bf611a69a4e434c606", null ]
];