var class_o_chunk =
[
    [ "OChunk", "class_o_chunk.html#a0a9b689572089aeb4e0cfbb2b9d7acb9", null ],
    [ "~OChunk", "class_o_chunk.html#a3b713a9902aa31f1147ac32999067eb4", null ],
    [ "GetID", "class_o_chunk.html#a5c2216184266de75b2e6f88982108198", null ],
    [ "GetParent", "class_o_chunk.html#aa020513f72feeb1fbbf14af682e24c5f", null ],
    [ "SetParent", "class_o_chunk.html#aae0d384d57f03507df2c80c267514b73", null ],
    [ "Size", "class_o_chunk.html#a9a9d495c7eaeb8202122ae07187efa29", null ],
    [ "Write", "class_o_chunk.html#ac05c2360b1fee4c217fdc98016ef5f9b", null ],
    [ "m_nID", "class_o_chunk.html#afc2b3f04ab2d95145665170466f7b83a", null ],
    [ "m_pParent", "class_o_chunk.html#abec32c4b0cfa5f3054f0a3ea7e1fbb98", null ]
];