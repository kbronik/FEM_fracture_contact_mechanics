var class_dense_matrix =
[
    [ "DenseMatrix", "class_dense_matrix.html#a962f1cca43a07753de96c8b9e413db06", null ],
    [ "~DenseMatrix", "class_dense_matrix.html#a7c920f60470d6a862624c7632fec5e4b", null ],
    [ "add", "class_dense_matrix.html#a98428819b12b22e0b385fd4865e4debf", null ],
    [ "Assemble", "class_dense_matrix.html#a379a9061560766fe091ab9e317549274", null ],
    [ "Assemble", "class_dense_matrix.html#ab7121805dd8ef207326c6ea085e1b94d", null ],
    [ "Clear", "class_dense_matrix.html#a9f0cb7e38e0ce89e500666859cb5cf1d", null ],
    [ "Create", "class_dense_matrix.html#abc4046bc136955ec65817a044ae4ffff", null ],
    [ "Create", "class_dense_matrix.html#a0a0109109f22e5e4e7b97b848c6792ea", null ],
    [ "diag", "class_dense_matrix.html#a38f23a8d1cc66a195a1bc3f981894163", null ],
    [ "operator()", "class_dense_matrix.html#aef1e1b632d6f76cae0eb8d94b58f19d4", null ],
    [ "set", "class_dense_matrix.html#ae14511ed82b0af0a5359d9177fed9630", null ],
    [ "m_pr", "class_dense_matrix.html#a642368c3d021519ec7fcbd3603a374e9", null ]
];