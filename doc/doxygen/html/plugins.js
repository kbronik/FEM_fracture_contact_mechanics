var plugins =
[
    [ "Plugin Basics", "plugin_basics.html", [
      [ "1. Building a plugin", "plugin_basics.html#plugin_sec1", [
        [ "Creating a plugin project in Visual Studio 2010", "plugin_basics.html#plugin_sec1_1", null ],
        [ "Creating a plugin for Linux", "plugin_basics.html#plugin_sec1_2", null ],
        [ "Creating a plugin for Mac", "plugin_basics.html#plugin_sec1_3", null ]
      ] ],
      [ "2. Required Functions", "plugin_basics.html#plugin_sec2", [
        [ "PluginInitialize", "plugin_basics.html#PluginInitialize", null ],
        [ "PluginNumClasses", "plugin_basics.html#PluginNumClasses", null ],
        [ "PluginGetFactory", "plugin_basics.html#PluginGetFactory", null ],
        [ "PluginCleanup", "plugin_basics.html#PluginCleanup", null ]
      ] ],
      [ "3. Factory Classes", "plugin_basics.html#plugin_sec3", null ],
      [ "4. Using the plugin", "plugin_basics.html#plugin_sec4", null ]
    ] ],
    [ "Material Plugins", "material.html", [
      [ "Basic Procedure", "material.html#mat_sec1", [
        [ "Defining the material base class", "material.html#step1", null ],
        [ "Registering the new material", "material.html#step2", null ],
        [ "Defining the material parameters", "material.html#step3", null ],
        [ "Implementing the initialization function", "material.html#step4", null ],
        [ "Implementing the stress and tangent functions", "material.html#step5", null ]
      ] ],
      [ "The FEElasticMaterialPoint class", "material.html#mat_sec2", null ],
      [ "Using the new material class", "material.html#mat_sec3", null ],
      [ "Debugging the Material Implementation", "material.html#mat_sec4", null ],
      [ "Advanced Topics", "material.html#mat_sec5", [
        [ "Array parameters", "material.html#subsec61", null ],
        [ "Uncoupled Materials", "material.html#subsec62", null ],
        [ "User-defined material points", "material.html#subsec63", null ]
      ] ]
    ] ]
];