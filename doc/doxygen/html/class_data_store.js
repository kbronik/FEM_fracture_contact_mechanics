var class_data_store =
[
    [ "DataStore", "class_data_store.html#a974a3aee6e9a1f8fbef3a31eec547ee5", null ],
    [ "~DataStore", "class_data_store.html#ad6ae5ab8bc74da0033abd61d119f2728", null ],
    [ "AddRecord", "class_data_store.html#af78c6585c27d3d497b01bdee30200feb", null ],
    [ "Clear", "class_data_store.html#a51b7f9fcce2279266afeac7f7c442365", null ],
    [ "GetDataRecord", "class_data_store.html#aa62826517e24a2c8d64593e038dfd3ab", null ],
    [ "Size", "class_data_store.html#a77ce091fb5f120585aeb1fd2a7e41b00", null ],
    [ "Write", "class_data_store.html#a773ac80fe449d27d4d0d8d7dead9a722", null ],
    [ "m_data", "class_data_store.html#af53f4c529733a5ff45b358e98199ba18", null ]
];