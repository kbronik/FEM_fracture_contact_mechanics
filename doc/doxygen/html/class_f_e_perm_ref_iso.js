var class_f_e_perm_ref_iso =
[
    [ "FEPermRefIso", "class_f_e_perm_ref_iso.html#aa7ab9bef95973624852f07516dc54c0b", null ],
    [ "DECLARE_PARAMETER_LIST", "class_f_e_perm_ref_iso.html#a0bcda5b4e49db17fe0d34fb8180f76d9", null ],
    [ "Init", "class_f_e_perm_ref_iso.html#aa08862ee8ce6742c7e451d4ca6c1fd03", null ],
    [ "Permeability", "class_f_e_perm_ref_iso.html#a090237802db9ed0eac38cd51801ff559", null ],
    [ "Tangent_Permeability_Strain", "class_f_e_perm_ref_iso.html#aa2266ef62f5db7f6715c1121f33b5f98", null ],
    [ "m_alpha", "class_f_e_perm_ref_iso.html#a183bbf672fb675fe9ca1a813ff5e9633", null ],
    [ "m_M", "class_f_e_perm_ref_iso.html#ae2ff46695c6f8171ea6156e9c9e18c17", null ],
    [ "m_perm0", "class_f_e_perm_ref_iso.html#aa58110f4754b5486ea3c50fe1827665a", null ],
    [ "m_perm1", "class_f_e_perm_ref_iso.html#a50a00914a78fd816653351215a056c82", null ],
    [ "m_perm2", "class_f_e_perm_ref_iso.html#a7788f6ba8a6157827c7b484ab02635b4", null ],
    [ "m_phi0", "class_f_e_perm_ref_iso.html#af028119b691662d9efc3fb131ef0e06d", null ]
];