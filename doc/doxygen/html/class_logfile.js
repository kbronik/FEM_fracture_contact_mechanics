var class_logfile =
[
    [ "MODE", "class_logfile.html#a3bdd9d37130efaceab9c8514a9fb8895", [
      [ "NEVER", "class_logfile.html#a3bdd9d37130efaceab9c8514a9fb8895a7a849b545fbcbae4831d3ec3cd0de2eb", null ],
      [ "FILE_ONLY", "class_logfile.html#a3bdd9d37130efaceab9c8514a9fb8895a214917b7b2901c241bda6ab915db4269", null ],
      [ "SCREEN_ONLY", "class_logfile.html#a3bdd9d37130efaceab9c8514a9fb8895a8d65dcbb435fcbdadfc985aa3c8f9bba", null ],
      [ "FILE_AND_SCREEN", "class_logfile.html#a3bdd9d37130efaceab9c8514a9fb8895a72ca52c016a9bfa2c50565ab0af7a347", null ]
    ] ],
    [ "~Logfile", "class_logfile.html#aa98d22586430a79ced1f6660d3a54f9b", null ],
    [ "append", "class_logfile.html#a15f980cc692c49d1653a7792a6cbe181", null ],
    [ "close", "class_logfile.html#a40a83f516f84b50d6d09b57904ad6999", null ],
    [ "FileName", "class_logfile.html#adf5cbe02a000a1c08cfe976fee3aeb44", null ],
    [ "flush", "class_logfile.html#a620e5c86b4e34c553d0639135ce3556b", null ],
    [ "GetMode", "class_logfile.html#a65eddad2064e9a450f6f4a76de8f1402", null ],
    [ "is_valid", "class_logfile.html#ab39820d03479efe8fbc32b19afd0dff7", null ],
    [ "open", "class_logfile.html#a1474647d887a4e63fce37e496ca20a57", null ],
    [ "operator FILE *", "class_logfile.html#af37925ed239b4cc3fe29b6281189a377", null ],
    [ "printbox", "class_logfile.html#aeee0c90858c23a025e14cf88ae9fa0b8", null ],
    [ "printf", "class_logfile.html#a49b1bcf95320f647a6dff09acaeaf165", null ],
    [ "SetLogStream", "class_logfile.html#a823a50cfb6dc337da42a611c496978ce", null ],
    [ "SetMode", "class_logfile.html#aa1f8be247352014dc8bc502374753750", null ],
    [ "m_fp", "class_logfile.html#a9147a4297a4651833a836d4e832ee8ed", null ],
    [ "m_mode", "class_logfile.html#a18940324fe44b5b45693d7bf8a04e7c2", null ],
    [ "m_ps", "class_logfile.html#a5e5e88570ec25f5ba045364f6594e14c", null ],
    [ "m_szfile", "class_logfile.html#a0bad7b967723d45be3f9fe5fd708e66b", null ]
];