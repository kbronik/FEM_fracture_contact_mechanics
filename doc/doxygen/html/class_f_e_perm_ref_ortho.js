var class_f_e_perm_ref_ortho =
[
    [ "FEPermRefOrtho", "class_f_e_perm_ref_ortho.html#a1752549684cb76ae99c8ac6bb83b5463", null ],
    [ "DECLARE_PARAMETER_LIST", "class_f_e_perm_ref_ortho.html#ab70f1a6707af5c9405622cb6df810456", null ],
    [ "Init", "class_f_e_perm_ref_ortho.html#abdafb27ce63e302d90d3d6fd0329a393", null ],
    [ "Permeability", "class_f_e_perm_ref_ortho.html#a0cd8fc9c8bc61c2f6afa9ed6dc494ead", null ],
    [ "Tangent_Permeability_Strain", "class_f_e_perm_ref_ortho.html#ad852a1df41385d6f1049eb0ac7444be1", null ],
    [ "m_alpha", "class_f_e_perm_ref_ortho.html#ad948634ec49f7acbc3c4aa7e17ca028e", null ],
    [ "m_alpha0", "class_f_e_perm_ref_ortho.html#a53483e7b4084b3ba990b2f9229fa28d8", null ],
    [ "m_M", "class_f_e_perm_ref_ortho.html#a4d06fc723e90ee140073f4dc8794a206", null ],
    [ "m_M0", "class_f_e_perm_ref_ortho.html#a25af758d16e95460940d30902e88f903", null ],
    [ "m_perm0", "class_f_e_perm_ref_ortho.html#a8083429ef556ac88ccbabcaf912f31c1", null ],
    [ "m_perm1", "class_f_e_perm_ref_ortho.html#a7ea9e745ea32092ff3abcfb218389096", null ],
    [ "m_perm2", "class_f_e_perm_ref_ortho.html#a785d9a39c085c7b396476a376b02025c", null ],
    [ "m_phi0", "class_f_e_perm_ref_ortho.html#a3de802b6d8446717d056fad3f000ee64", null ]
];