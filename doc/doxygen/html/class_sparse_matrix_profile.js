var class_sparse_matrix_profile =
[
    [ "SparseMatrixProfile", "class_sparse_matrix_profile.html#a2c70006ae87d78a60b03616317eb2cca", null ],
    [ "~SparseMatrixProfile", "class_sparse_matrix_profile.html#ad2a238f0c36370328d1c5c09252b311f", null ],
    [ "SparseMatrixProfile", "class_sparse_matrix_profile.html#ab4d5c0e173b7dc3f19d782cc45f23aea", null ],
    [ "clear", "class_sparse_matrix_profile.html#a7f0a7912e0cfe9e35bc112cbf142890a", null ],
    [ "column", "class_sparse_matrix_profile.html#ad572b9afeb17ca6a795818b3eb616e03", null ],
    [ "operator=", "class_sparse_matrix_profile.html#a8a2d2cf6dd25272d9b6390b2e17a5e5d", null ],
    [ "size", "class_sparse_matrix_profile.html#a968f2883eedb661f8e279302c680b22f", null ],
    [ "UpdateProfile", "class_sparse_matrix_profile.html#afcce4b0bec6fa940f170cb26c076a05e", null ],
    [ "m_prof", "class_sparse_matrix_profile.html#aac55f7526f074fd69283c991bec5cce0", null ]
];