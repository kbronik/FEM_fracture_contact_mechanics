var class_f_e_plane =
[
    [ "FEPlane", "class_f_e_plane.html#aced9edb99da76251ea154d0ace906258", null ],
    [ "DECLARE_PARAMETER_LIST", "class_f_e_plane.html#a9872c5916ee0730d72905f8de9eeefe2", null ],
    [ "GetEquation", "class_f_e_plane.html#a4ce30586623821e8e5dbd7cd28e67b38", null ],
    [ "Init", "class_f_e_plane.html#a1c25420b44f2337e399d2be1258d910f", null ],
    [ "Normal", "class_f_e_plane.html#a525265f39b353e705243871c18c60e72", null ],
    [ "Project", "class_f_e_plane.html#a2be8782a3329c9895ba766f3c1b7a2dc", null ],
    [ "SetParameterAttribute", "class_f_e_plane.html#af823b94ac7d9f7794565e3b4ce758c04", null ],
    [ "a", "class_f_e_plane.html#a865d4b20817c9a5d2c737685aca2d513", null ],
    [ "m_nplc", "class_f_e_plane.html#aed41c433db6276c5aa9a9bcad82c536e", null ],
    [ "m_pplc", "class_f_e_plane.html#adec3aa3c08293a231a6e008d066972ab", null ]
];