var class_f_e_tied_biphasic_surface =
[
    [ "FETiedBiphasicSurface", "class_f_e_tied_biphasic_surface.html#ada2a5784d09ce1ab0aab10c094e89832", null ],
    [ "Init", "class_f_e_tied_biphasic_surface.html#a182edc58c0f87d7699e366de219c339d", null ],
    [ "Serialize", "class_f_e_tied_biphasic_surface.html#a2fa1edf61d4f77bbcaa86de200e8e14e", null ],
    [ "SetPoroMode", "class_f_e_tied_biphasic_surface.html#a7c5b6ca1ca51e9c486a6757b48e3dd32", null ],
    [ "ShallowCopy", "class_f_e_tied_biphasic_surface.html#a133af93ecbd696d90a3d7c498f757c6c", null ],
    [ "UpdateNodeNormals", "class_f_e_tied_biphasic_surface.html#ae6b76372ffa690aad9beb5321fb87fd9", null ],
    [ "m_bporo", "class_f_e_tied_biphasic_surface.html#a1060688c2034326f6d280212d3014eeb", null ],
    [ "m_dg", "class_f_e_tied_biphasic_surface.html#a048d03195f29a533298a97d8cd51b9ae", null ],
    [ "m_epsn", "class_f_e_tied_biphasic_surface.html#a32cf889d0800df4726ebc4fe3b4236a0", null ],
    [ "m_epsp", "class_f_e_tied_biphasic_surface.html#a28f996fb2a3336004081ceeeeed35fa5", null ],
    [ "m_Gap", "class_f_e_tied_biphasic_surface.html#a410eb046ba624ef15a8633324717bba0", null ],
    [ "m_Lmd", "class_f_e_tied_biphasic_surface.html#a9f63bd67dcba15883561176a2724f1e2", null ],
    [ "m_Lmp", "class_f_e_tied_biphasic_surface.html#ab7d0e00ead46c7649955d183b821121f", null ],
    [ "m_nei", "class_f_e_tied_biphasic_surface.html#a4faf5b246f1c65f5ccbe44d2e540aadd", null ],
    [ "m_nn", "class_f_e_tied_biphasic_surface.html#a80145bebe0e9defa409df4a8c5fe5120", null ],
    [ "m_nu", "class_f_e_tied_biphasic_surface.html#a3b6997ff8a7b6098ca80eecdbce61deb", null ],
    [ "m_pfem", "class_f_e_tied_biphasic_surface.html#aba26e42f8a6ef29096a668b626694b7d", null ],
    [ "m_pg", "class_f_e_tied_biphasic_surface.html#a71f8f6332b5a241080811a288c62bb60", null ],
    [ "m_pme", "class_f_e_tied_biphasic_surface.html#ad95a1376ef5cb318b266cfb27b08992f", null ],
    [ "m_poro", "class_f_e_tied_biphasic_surface.html#a3fc171494b22aff826f0e82d60e2e5a6", null ],
    [ "m_rs", "class_f_e_tied_biphasic_surface.html#a2579f8340bdd09fcb596ba95c4914063", null ]
];