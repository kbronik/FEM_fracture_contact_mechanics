var class_d_o_f_s =
[
    [ "~DOFS", "class_d_o_f_s.html#a79771f97ad17a7a3c2358cd1ef1792bd", null ],
    [ "GetCDOFS", "class_d_o_f_s.html#ab63f57d2d80310378991d31210f922a5", null ],
    [ "GetNDOFS", "class_d_o_f_s.html#ad8caff7e275080ae9053a5c516d9128e", null ],
    [ "Reset", "class_d_o_f_s.html#aab270e1112255026ecd03619e6c49e03", null ],
    [ "SetCDOFS", "class_d_o_f_s.html#a3921fd231eb23e5dbf4416995b07d67f", null ],
    [ "SetNDOFS", "class_d_o_f_s.html#a35cb71e4b317fbb02fd6bb2987f975bc", null ]
];