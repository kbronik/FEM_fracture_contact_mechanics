#pragma once

//-----------------------------------------------------------------------------
//! The FEBioMech module

//! This module defines classes for dealing with large deformation structural
//! mechanics problems. 

namespace FEBioMech
{
	//! Initialize the FEBioMech module
	void InitModule();
}
